package com.example.buildit.invoices.rest;

import com.example.buildit.invoices.application.dto.CreateInvoiceRequestDTO;
import com.example.buildit.invoices.application.dto.InvoiceDTO;
import com.example.buildit.invoices.application.services.InvoiceService;
import com.example.buildit.purchase.application.dto.PurchaseOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.util.Arrays;

@RestController
@RequestMapping("/api/invoices")
public class InvoicesController {

    @Autowired
    InvoiceService invoiceService;

    @Value("${rentit.URL.api}")
    String rentitURLapi;

	/*
	Postman:
		POST request
		Body:
			{
				"purchaseOrderReference":"1",
					"dueDate":"2020-07-02",
					"amount":"30"
			}
	*/

    @PostMapping("/create")
    public ResponseEntity createInvoice(@RequestBody CreateInvoiceRequestDTO createInvoiceDTO) {
        try {
            InvoiceDTO invoiceDTO = invoiceService.createInvoice(createInvoiceDTO);

            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(invoiceDTO.getRequiredLink(IanaLinkRelations.SELF).toUri());

            return new ResponseEntity<>(invoiceDTO, headers, HttpStatus.CREATED);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("")
    public CollectionModel<InvoiceDTO> getAllInvoices() throws Exception {
        try {
            return invoiceService.all();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get invoices", e);
        }
    }

    @PostMapping("/{id}/accept")
    public InvoiceDTO acceptPurchaseOrder(@PathVariable Long id) {
        try {
            return invoiceService.acceptInvoice(id);
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    @PostMapping("/{id}/reject")
    public InvoiceDTO rejectPurchaseOrder(@PathVariable Long id) {
        try {
            return invoiceService.rejectInvoice(id);
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    @GetMapping("/checkPOInReceivedInvoice")
    public ResponseEntity checkPOInReceivedInvoice(@RequestParam("poNumber") Long poId) throws Exception {
        String url = rentitURLapi + "/sales/orders/" + poId;

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
        // Request to return JSON format
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<PurchaseOrderDTO> entity = new HttpEntity<PurchaseOrderDTO>(headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<PurchaseOrderDTO> response = null;
        try {
            response = restTemplate.exchange(url, HttpMethod.GET, entity, PurchaseOrderDTO.class);
        } catch (HttpClientErrorException ex) {
            return new ResponseEntity<String>(ex.getResponseBodyAsString(), ex.getResponseHeaders(), ex.getStatusCode());
        } catch (Exception e) {
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        if (response.getStatusCode() == HttpStatus.OK) {
            PurchaseOrderDTO purchaseOrderDTO = response.getBody();
            boolean valid = invoiceService.isPOUnpaid(purchaseOrderDTO);
            if (valid) {
                return new ResponseEntity<>("Invoice is valid.", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Invoice is not valid.", HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>("Check failed.", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/{id}/remittance")
    public InvoiceDTO createRemittance(@PathVariable Long id) {
        try {
            return invoiceService.createRemittance(id);
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    @PostMapping("/paymentReminder/{poid}")
    public ResponseEntity paymentReminder(@PathVariable("poid") Long poid) throws Exception {
        InvoiceDTO invoiceDTO = invoiceService.remind(poid);
        if (invoiceDTO != null) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
        }
    }
}
