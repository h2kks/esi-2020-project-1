package com.example.buildit.invoices.domain.model;

import com.example.buildit.purchase.domain.model.PlantHireRequest;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne
    PlantHireRequest plantRequest;

    LocalDate dueDate;
    BigDecimal amount;
    InvoiceStatus status;
    String purchaseOrderReference;
    LocalDate paymentDate;

    public static Invoice of(PlantHireRequest plantRequest, LocalDate dueDate, BigDecimal amount, String purchaseOrderReference) {
        Invoice invoice = new Invoice();
        invoice.setPlantRequest(plantRequest);
        invoice.setDueDate(dueDate);
        invoice.setAmount(amount);
        invoice.setPurchaseOrderReference(purchaseOrderReference);
        return invoice;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }
}
