package com.example.buildit.invoices.application.services;

import com.example.buildit.invoices.application.dto.CreateInvoiceRequestDTO;
import com.example.buildit.invoices.application.dto.InvoiceDTO;
import com.example.buildit.invoices.domain.model.Invoice;
import com.example.buildit.invoices.domain.model.InvoiceStatus;
import com.example.buildit.invoices.domain.repository.InvoiceRepository;
import com.example.buildit.purchase.application.dto.PurchaseOrderDTO;
import com.example.buildit.purchase.domain.model.PlantHireRequest;
import com.example.buildit.purchase.domain.repository.PlantHireRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InvoiceService {

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    PlantHireRequestRepository plantHireRequestRepository;

    @Autowired
    InvoiceRepository invoiceRepository;

    public InvoiceDTO createInvoice(CreateInvoiceRequestDTO invoiceRequestDTO) throws Exception {
        List<PlantHireRequest> plantHireRequests = plantHireRequestRepository.findPlantRequestsByPOReference(invoiceRequestDTO.getPurchaseOrderReference());
        if (plantHireRequests.size() == 0) {
            throw new Exception("Plant Hire Request not found.");
        }
        PlantHireRequest plantHireRequest = plantHireRequests.get(0);
        plantHireRequest.setPurchaseOrderReference(invoiceRequestDTO.getPurchaseOrderReference());
        Invoice invoice = Invoice.of(plantHireRequest, invoiceRequestDTO.getDueDate(), invoiceRequestDTO.getAmount(), invoiceRequestDTO.getPurchaseOrderReference());
        invoice.setStatus(InvoiceStatus.UNPAID);
        invoice.setPlantRequest(plantHireRequest);
        invoiceRepository.save(invoice);
        plantHireRequest.setInvoice(invoice);
        plantHireRequestRepository.save(plantHireRequest);
        return invoiceAssembler.toModel(invoice);
    }

    public CollectionModel<InvoiceDTO> all() throws Exception {
        return invoiceAssembler.toCollectionModel(invoiceRepository.findAll());
    }

    public InvoiceDTO acceptInvoice(Long id) throws Exception {
        Invoice invoice = invoiceRepository.findById(id).orElse(null);

        if (invoice == null) {
            throw new Exception("Can't find Invoice");
        }

        invoice.setStatus(InvoiceStatus.APPROVED);
        invoiceRepository.save(invoice);

        return invoiceAssembler.toModel(invoice);
    }

    public InvoiceDTO rejectInvoice(Long id) throws Exception {
        Invoice invoice = invoiceRepository.findById(id).orElse(null);

        if (invoice == null) {
            throw new Exception("Can't find Invoice");
        }

        invoice.setStatus(InvoiceStatus.REJECTED);
        invoiceRepository.save(invoice);

        return invoiceAssembler.toModel(invoice);
    }

    public boolean isPOUnpaid(PurchaseOrderDTO purchaseOrderDTO) {
        if (purchaseOrderDTO.getInvoice() != null && purchaseOrderDTO.getInvoice().getStatus() != InvoiceStatus.PAID) {
            return true;
        } else {
            return false;
        }
    }

    public InvoiceDTO createRemittance(Long id) throws Exception {
        Invoice invoice = invoiceRepository.findById(id).orElse(null);

        if (invoice == null) {
            throw new Exception("Can't find Invoice");
        }

        if (invoice.getStatus() != InvoiceStatus.APPROVED) {
            throw new Exception("Invoice status should be approved");
        }

        invoice.setStatus(InvoiceStatus.PAID);
        invoice.setPaymentDate(LocalDate.now());

        invoiceRepository.save(invoice);

        return invoiceAssembler.toModel(invoice);
    }

    public InvoiceDTO remind(Long poid) throws Exception {
        List<PlantHireRequest> plantHireRequests = plantHireRequestRepository.findPlantRequestsByPOReference(String.valueOf(poid));
        if (plantHireRequests.size() == 0) {
            throw new Exception("Plant Hire Request not found.");
        }
        PlantHireRequest plantHireRequest = plantHireRequests.get(0);
        plantHireRequest.getInvoice().setStatus(InvoiceStatus.REMINDED);
        invoiceRepository.save(plantHireRequest.getInvoice());
        return invoiceAssembler.toModel(plantHireRequest.getInvoice());
    }
}
