package com.example.buildit.purchase.application.dto;

import com.example.buildit.common.application.dto.BusinessPeriodDTO;
import com.example.buildit.invoices.application.dto.InvoiceDTO;
import com.example.buildit.purchase.domain.model.RequestStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.math.BigDecimal;

@Data
public class PlantHireRequestDTO extends RepresentationModel<PlantHireRequestDTO> {

    Long _id;
    String siteEngineerName;
    String siteId;
    String supplier;
    String plantReference;
    BusinessPeriodDTO rentalPeriod;
    RequestStatus status;
    BigDecimal price;
    InvoiceDTO invoice;
    String purchaseOrderReference;
}
