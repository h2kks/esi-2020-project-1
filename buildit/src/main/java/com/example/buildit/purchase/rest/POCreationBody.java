package com.example.buildit.purchase.rest;

import java.time.LocalDate;

public class POCreationBody {
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getPlantId() {
        return plantId;
    }

    public void setPlantId(Long plantId) {
        this.plantId = plantId;
    }

    private LocalDate startDate;
    private LocalDate endDate;
    private Long plantId;

    @Override
    public String toString() {
        return "CreationBody{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                ", plantId=" + plantId +
                '}';
    }


}
