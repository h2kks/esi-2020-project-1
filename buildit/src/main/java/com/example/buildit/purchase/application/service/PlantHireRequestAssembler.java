package com.example.buildit.purchase.application.service;

import com.example.buildit.common.application.dto.BusinessPeriodDTO;
import com.example.buildit.invoices.application.services.InvoiceAssembler;
import com.example.buildit.purchase.application.dto.PlantHireRequestDTO;
import com.example.buildit.purchase.domain.model.PlantHireRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantHireRequestAssembler extends RepresentationModelAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {

    @Autowired
    InvoiceAssembler invoiceAssembler;
    
	public PlantHireRequestAssembler() {
        super(PlantHireRequestAssembler.class, PlantHireRequestDTO.class);
    }

    @Override
    public PlantHireRequestDTO toModel(PlantHireRequest plantHireRequest) {
        PlantHireRequestDTO dto = createModelWithId(plantHireRequest.getId(), plantHireRequest);
        dto.set_id(plantHireRequest.getId());
        dto.setSiteEngineerName(plantHireRequest.getSiteEngineerName());
        dto.setSiteId(plantHireRequest.getSiteId());
        dto.setSupplier(plantHireRequest.getSupplier());
        dto.setPlantReference(plantHireRequest.getPlantReference());
        dto.setRentalPeriod(BusinessPeriodDTO.of(plantHireRequest.getRentalPeriod().getStartDate(), plantHireRequest.getRentalPeriod().getEndDate()));
        dto.setStatus(plantHireRequest.getStatus());
        dto.setPrice(plantHireRequest.getPrice());
        dto.setPurchaseOrderReference(plantHireRequest.getPurchaseOrderReference());
//        dto.setInvoice(invoiceAssembler.toModel(plantHireRequest.getInvoice()));
        return dto;
    }
}
