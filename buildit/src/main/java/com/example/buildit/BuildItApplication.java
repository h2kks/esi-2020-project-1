package com.example.buildit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BuildItApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildItApplication.class, args);
    }

}
