import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import Plants from '../components/Plants'
import NewMaintenanceTask from '../components/NewMaintenanceTask'
import NewPurchaseOrder from '../components/NewPurchaseOrder'
import PurchaseOrder from '../components/PurchaseOrder'
import PurchaseOrders from '../components/PurchaseOrders'
import CreateInvoice from '../components/CreateInvoice'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/plants',
      name: 'Plants',
      component: Plants
    },
    {
      path: '/purchase-orders',
      name: 'PurchaseOrders',
      component: PurchaseOrders
    },
    {
      path: '/new-maintenance/:id',
      name: 'NewMaintenanceTask',
      component: NewMaintenanceTask
    },
    {
      path: '/purchase-orders/:id',
      name: 'PurchaseOrder',
      component: PurchaseOrder
    },
    {
      path: '/new-purchase-order',
      name: 'NewPurchaseOrder',
      component: NewPurchaseOrder
    },
    {
      path: '/create-invoice/:id',
      name: 'CreateInvoice',
      component: CreateInvoice
    }
  ]
})
