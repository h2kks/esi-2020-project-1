import Vue from 'vue'

export default {
  showErrorPopup: function (title, text) {
    this.showPopup(title, text, 'error')
  },
  showSuccessPopup: function (title, text) {
    this.showPopup(title, text, 'success')
  },
  showInfoPopup: function (title, text) {
    this.showPopup(title, text, 'info')
  },
  showPopup: function (title, text, type) {
    Vue.notify({
      group: 'rentit-messages',
      type: type,
      title: title,
      text: text
    })
  }
}
