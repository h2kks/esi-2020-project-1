package com.example.rentit.invoices.rest;

import com.example.rentit.RentItApplication;
import com.example.rentit.invoices.application.dto.InvoiceDTO;
import com.example.rentit.invoices.domain.model.InvoiceStatus;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class)
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class InvoiceControllerTests {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    ObjectMapper mapper;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    //PS13
    @Test
    public void testCreateInvoice() throws Exception {

        mockMvc.perform(get("/api/invoices/createByPOid").param("poId", "1").param("dueDate", "2020-09-02"))
                .andExpect(status().isCreated());


        assertThat(checkInvoices().size()).isEqualTo(1);
    }

    //PS13
    public List<InvoiceDTO> checkInvoices() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/invoices").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        JsonNode invoicesJSON = mapper.readTree(result.getResponse().getContentAsString())
                .path("_embedded")
                .path("invoiceDToes");
        List<InvoiceDTO> invoices = mapper.readValue(invoicesJSON.toString(), new TypeReference<List<InvoiceDTO>>() {
        });
        return invoices;
    }
    
    @Test
    public void testSubmitRemittanceAdvice()  throws Exception {
        mockMvc.perform(get("/api/invoices/createByPOid").param("poId", "1").param("dueDate", "2020-09-02"))
        .andExpect(status().isCreated());
        
        List<InvoiceDTO> invoices = checkInvoices();
        InvoiceDTO invoice = invoices.get(0);

        MvcResult result = mockMvc.perform(post("/api/invoices/"+invoice.get_id()+"/submitRemittanceAdvice")
                .contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        
        InvoiceDTO returnedInvoice = mapper.readValue(result.getResponse().getContentAsString(), InvoiceDTO.class);
        assertEquals(InvoiceStatus.PAID, returnedInvoice.getStatus());
    }
}
