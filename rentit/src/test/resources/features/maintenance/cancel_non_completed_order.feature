Feature: Cancel non-completed maintenance order
  As a site engineer
  So that I know the status of an order
  I want to be able to query order data

  Background: Maintenance order catalog
    Given the following inventory entry catalog
      | id | name           | description              | price |
      | 1  | Mini excavator | 1.5 Tonne Mini excavator | 150   |
    And the following inventory item catalog
      | id | plantInfo | serialNumber | equipmentCondition      |
      | 1  | 1         | A01          | SERVICEABLE             |
      | 2  | 2         | A02          | SERVICEABLE             |
      | 2  | 3         | A03          | UNSERVICEABLEREPAIRABLE |
    And the following maintenance order catalog
      | id | maintenancePeriod        | plant | status    | site        | description      | engineerName      |
      | 1  | (2021-02-02, 2021-02-03) | 1     | PENDING   | Delta hoone | Valdeki tellimus | Valdek Kontroller |
      | 2  | (2021-02-03, 2021-02-03) | 1     | COMPLETED | Delta hoone | Valduri tellimus | Valdur Propeller  |

  Scenario: Querying the maintenance order for with id 1
    When the site engineer queries the maintenance order with id 1
    Then the maintenance order information and relevant information is shown

  Scenario: Requesting the cancellation of an order with id 1
    When the site engineer cancels the order
    Then the order is successfully cancelled

  Scenario: Querying the maintenance order for with id 1
    When the site engineer queries the maintenance order with id 1
    Then the maintenance order information is shown and the status is cancelled