Feature: Create maintenance order
  As a Rentit's customer
  So that I start with the construction project
  I want hire all the required machinery

  Background: Maintenance order catalog
    Given the following inventory entry catalog
      | id | name           | description              | price |
      | 1  | Mini excavator | 1.5 Tonne Mini excavator | 150   |
    And the following inventory item catalog
      | id | plantInfo | serialNumber | equipmentCondition      |
      | 1  | 1         | A01          | SERVICEABLE             |
      | 2  | 2         | A02          | SERVICEABLE             |
      | 2  | 3         | A03          | UNSERVICEABLEREPAIRABLE |
    And the following maintenance order catalog
      | id | maintenancePeriod        | plant | status    | site        | description      | engineerName      |
      | 1  | (2021-02-02, 2021-02-03) | 1     | PENDING   | Delta hoone | Valdeki tellimus | Valdek Kontroller |
      | 2  | (2021-02-03, 2021-02-03) | 1     | COMPLETED | Delta hoone | Valduri tellimus | Valdur Propeller  |

  Scenario: Creating a maintenance order
    When the customer adds a new maintenance order with the following data: start_date: "2021-03-24", end_date: "2021-03-24", plant_id: "1", site: "Delta hoone", description: "Vahuri tellimus", engineer_name: "Vahur"
    Then a new maintenance order is created a reference to the order is returned