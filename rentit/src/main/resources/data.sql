insert into plant_inventory_entry (id, name, description, price) values (1, 'Mini excavator', '1.5 Tonne Mini excavator', 150);
insert into plant_inventory_entry (id, name, description, price) values (2, 'Mini excavator', '3 Tonne Mini excavator', 200);
insert into plant_inventory_entry (id, name, description, price) values (3, 'Midi excavator', '5 Tonne Midi excavator', 250);
insert into plant_inventory_entry (id, name, description, price) values (4, 'Midi excavator', '8 Tonne Midi excavator', 300);
insert into plant_inventory_entry (id, name, description, price) values (5, 'Maxi excavator', '15 Tonne Large excavator', 400);
insert into plant_inventory_entry (id, name, description, price) values (6, 'Maxi excavator', '20 Tonne Large excavator', 450);
insert into plant_inventory_entry (id, name, description, price) values (7, 'HS dumper', '1.5 Tonne Hi-Swivel Dumper', 150);
insert into plant_inventory_entry (id, name, description, price) values (8, 'FT dumper', '2 Tonne Front Tip Dumper', 180);
insert into plant_inventory_entry (id, name, description, price) values (9, 'FT dumper', '2 Tonne Front Tip Dumper', 200);
insert into plant_inventory_entry (id, name, description, price) values (10, 'FT dumper', '2 Tonne Front Tip Dumper', 300);
insert into plant_inventory_entry (id, name, description, price) values (11, 'FT dumper', '3 Tonne Front Tip Dumper', 400);
insert into plant_inventory_entry (id, name, description, price) values (12, 'Loader', 'Hewden Backhoe Loader', 200);
insert into plant_inventory_entry (id, name, description, price) values (13, 'D-Truck', '15 Tonne Articulating Dump Truck', 250);
insert into plant_inventory_entry (id, name, description, price) values (14, 'D-Truck', '30 Tonne Articulating Dump Truck', 300);

insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (1, 1, 'A01', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (2, 2, 'A02', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (3, 3, 'A03', 'UNSERVICEABLEREPAIRABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (4, 4, 'A04', 'SERVICEABLE');
insert into plant_inventory_item (id, plant_info_id, serial_number, equipment_condition) values (5, 5, 'A05', 'SERVICEABLE');

insert into maintenance_order (id, start_date, end_date, plant_id, status, site, description, engineer_name) values (1, '2021-03-24', '2021-03-24', 1, 'PENDING', 'Delta hoone', 'Valdeki tellimus', 'Valdek Kontroller');
insert into maintenance_order (id, start_date, end_date, plant_id, status, site, description, engineer_name) values (2, '2017-03-22', '2017-03-24', 1, 'COMPLETED', 'Delta hoone', 'Valduri tellimus', 'Valdur Propeller');

insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (1, '2017-03-22', '2017-03-24', 1, 'ACCEPTED', '2018-03-22', '2018-03-24');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (2, '2017-03-22', '2017-03-24', 1, 'ACCEPTED', '2017-04-22', '2017-04-24');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (3, '2021-04-18', '2021-05-22', 1, 'ACCEPTED', '2021-04-18', '2021-04-22');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (4, '2017-03-22', '2017-03-24', 1, 'ACCEPTED', '2017-06-22', '2017-06-24');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (5, '2017-03-22', '2017-03-24', 1, 'ACCEPTED', '2017-06-22', '2017-06-24');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (6, '2017-03-22', '2017-03-24', 4, 'PLANT_DELIVERED', '2020-05-12', '2022-06-24');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (7, '2017-03-22', '2017-03-24', 5, 'PLANT_DISPATCHED', '2020-05-12', '2022-06-24');
insert into purchase_order (id, issue_date, payment_schedule, plant_id, status, start_date, end_date) values (8, '2021-03-22', '2021-03-24', 1, 'PENDING', '2021-03-22', '2021-03-24');

insert into plant_reservation(id, start_date, end_date, rental_id, plant_id) values (1000, '2021-04-18', '2021-04-22', 1, 3);
insert into plant_reservation(id, start_date, end_date, rental_id, plant_id) values (1001, '2020-05-12', '2022-06-24', 6, 4);
insert into plant_reservation(id, start_date, end_date, rental_id, plant_id) values (1002, '2020-05-12', '2022-06-24', 7, 5);
insert into plant_reservation(id, start_date, end_date, rental_id, plant_id) values (1003, '2021-03-22', '2021-03-24', 8, 1);
insert into plant_reservation(id, start_date, end_date, rental_id, plant_id) values (1004, '2021-04-18', '2021-04-22', 3, 1);

insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, reservation_id)
    values (1, 'first desc', 'PREVENTIVE', 100, {ts '2022-01-18 18:47:52.69'}, {ts '2022-01-20 18:47:52.69'}, 1);

insert into maintenance_task (id, description, type_of_work, price, start_date, end_date, reservation_id)
    values (2, 'first desc', 'PREVENTIVE', 100, {ts '2021-03-18 18:47:52.69'}, {ts '2021-03-27 18:47:52.69'}, 2);
