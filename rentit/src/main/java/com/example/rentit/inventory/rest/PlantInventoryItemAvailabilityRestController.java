package com.example.rentit.inventory.rest;

import com.example.rentit.inventory.application.service.PlantInventoryItemAvailabilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/inventory/items")
public class PlantInventoryItemAvailabilityRestController {
    @Autowired
    PlantInventoryItemAvailabilityService plantInventoryItemAvailabilityService;

    @GetMapping("/{id}/availability")
    public boolean isPlantAvailable(
            @PathVariable("id") Long id,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        if (endDate.isBefore(startDate)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could startDate can not be after endDate");
        }
        try {
            return plantInventoryItemAvailabilityService.isAvailable(id, startDate, endDate);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Error: could not get availability information", e);
        }
    }

}
