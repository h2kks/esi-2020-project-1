package com.example.rentit.inventory.domain.repository;

import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

@Service
public class CustomPlantInventoryEntryRepositoryImpl implements CustomPlantInventoryEntryRepository {

    @Autowired
    EntityManager em;

    @Override
    public boolean getEntryAvailabilityById(Long id, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p from PlantInventoryItem p where p.plantInfo.id = ?1 and p not in " +
                        "(select r.plant from PlantReservation r where not ((r.schedule.startDate > ?3 and r.schedule.endDate > ? 3) or (r.schedule.startDate < ?2 and r.schedule.endDate < ? 2))) and p not in (select m.reservation from MaintenanceTask m where not ((m.maintenancePeriod.startDate > ?3 and m.maintenancePeriod.endDate > ? 3) or (m.maintenancePeriod.startDate < ?2 and m.maintenancePeriod.endDate < ? 2)))",
                PlantInventoryItem.class)
                .setParameter(1, id)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList().size() > 0;
    }

    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p.plantInfo from PlantInventoryItem p where p.plantInfo.name like concat('%', ?1, '%') and p not in" +
                        "(select r.plant from PlantReservation r where not ((r.schedule.startDate > ?3 and r.schedule.endDate > ? 3) or (r.schedule.startDate < ?2 and r.schedule.endDate < ? 2))) and p not in (select m.reservation from MaintenanceTask m where not ((m.maintenancePeriod.startDate > ?3 and m.maintenancePeriod.endDate > ? 3) or (m.maintenancePeriod.startDate < ?2 and m.maintenancePeriod.endDate < ? 2)))",
                PlantInventoryEntry.class)
                .setParameter(1, name)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

}
