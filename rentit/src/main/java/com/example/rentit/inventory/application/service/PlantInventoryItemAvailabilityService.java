package com.example.rentit.inventory.application.service;

import com.example.rentit.inventory.domain.repository.CustomPlantInventoryItemRepository;
import com.example.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class PlantInventoryItemAvailabilityService {

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    CustomPlantInventoryItemRepository customPlantInventoryItemRepository;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public boolean isAvailable(long id, LocalDate startDate, LocalDate endDate) {
        return customPlantInventoryItemRepository.getItemAvailabilityById(id, startDate, endDate);
    }
}
