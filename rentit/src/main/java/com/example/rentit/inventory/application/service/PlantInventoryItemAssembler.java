package com.example.rentit.inventory.application.service;


import com.example.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import com.example.rentit.inventory.rest.PlantInventoryEntryRestController;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryItemAssembler extends RepresentationModelAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {

    public PlantInventoryItemAssembler() {
        super(PlantInventoryEntryRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toModel(PlantInventoryItem plantInventoryItem) {
        PlantInventoryItemDTO dto = createModelWithId(plantInventoryItem.getId(), plantInventoryItem);
        dto.set_id(plantInventoryItem.getId());
        dto.setSerialNumber(plantInventoryItem.getSerialNumber());
        dto.setEquipmentCondition(plantInventoryItem.getEquipmentCondition());

        return dto;
    }

}
