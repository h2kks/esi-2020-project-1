package com.example.rentit.invoices.rest;

import java.math.BigDecimal;
import java.time.LocalDate;

public class InvoiceCreationBody {

    public String getPurchaseOrderReference() {
        return purchaseOrderReference;
    }

    public void setPurchaseOrderReference(String purchaseOrderReference) {
        this.purchaseOrderReference = purchaseOrderReference;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    String purchaseOrderReference;
    LocalDate dueDate;
    BigDecimal amount;

    @Override
    public String toString() {
        return "InvoiceCreationBody{" +
                "purchaseOrderReference=" + purchaseOrderReference +
                ", dueDate=" + dueDate +
                ", amount=" + amount +
                '}';
    }
}
