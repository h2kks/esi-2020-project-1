package com.example.rentit.invoices.application.service;

import com.example.rentit.invoices.application.dto.InvoiceDTO;
import com.example.rentit.invoices.domain.model.Invoice;
import com.example.rentit.sales.application.services.PurchaseOrderAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;


@Service
public class InvoiceAssembler extends RepresentationModelAssemblerSupport<Invoice, InvoiceDTO> {

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    public InvoiceAssembler() {
        super(InvoiceAssembler.class, InvoiceDTO.class);
    }

    @Override
    public InvoiceDTO toModel(Invoice invoice) {
        InvoiceDTO dto = createModelWithId(invoice.getId(), invoice);
        dto.set_id(invoice.getId());
//        dto.setPurchaseOrder(purchaseOrderAssembler.toModel(invoice.getPurchaseOrder()));
        dto.setDueDate(invoice.getDueDate());
        dto.setAmount(invoice.getAmount());
        dto.setStatus(invoice.getStatus());
        return dto;
    }
}
