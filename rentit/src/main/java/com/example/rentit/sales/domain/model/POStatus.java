package com.example.rentit.sales.domain.model;

public enum POStatus {PENDING, ACCEPTED, PLANT_DELIVERED, REJECTED, REJECTED_BY_CUSTOMER, PLANT_DISPATCHED, PLANT_RETURNED, INVOICED, CANCELLED}