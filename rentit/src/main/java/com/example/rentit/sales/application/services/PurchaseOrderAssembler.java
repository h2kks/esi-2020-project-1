package com.example.rentit.sales.application.services;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.rentit.invoices.application.service.InvoiceAssembler;
import com.example.rentit.sales.application.dto.PurchaseOrderDTO;
import com.example.rentit.sales.domain.model.PurchaseOrder;
import com.example.rentit.sales.rest.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class PurchaseOrderAssembler extends RepresentationModelAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {

    public PurchaseOrderAssembler() {
        super(PurchaseOrderRestController.class, PurchaseOrderDTO.class);
    }

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Override
    public PurchaseOrderDTO toModel(PurchaseOrder purchaseOrder) {
        PurchaseOrderDTO dto = createModelWithId(purchaseOrder.getId(), purchaseOrder);
        dto.set_id(purchaseOrder.getId());
        dto.setStatus(purchaseOrder.getStatus());

        dto.setRentalPeriod(BusinessPeriodDTO.of(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));

        dto.setPlantId(purchaseOrder.getPlant().getId());

        dto.add(linkTo(methodOn(PurchaseOrderRestController.class)
                .fetchPurchaseOrder(dto.get_id())).withRel("fetch"));

        if (purchaseOrder.getInvoice() != null) {
            dto.setInvoice(invoiceAssembler.toModel(purchaseOrder.getInvoice()));
        }
        try {
            switch (purchaseOrder.getStatus()) {
                case PENDING:
                    dto.add(linkTo(methodOn(PurchaseOrderRestController.class)
                            .acceptPurchaseOrder(dto.get_id())).withRel("accept")
                            .withType(HttpMethod.POST.toString()));
                    dto.add(linkTo(methodOn(PurchaseOrderRestController.class)
                            .rejectPurchaseOrder(dto.get_id())).withRel("reject")
                            .withType(HttpMethod.DELETE.toString()));
                    dto.add(linkTo(methodOn(PurchaseOrderRestController.class)
                            .rejectPurchaseOrder(dto.get_id())).withRel("dispatch")
                            .withType(HttpMethod.POST.toString()));
                    break;
                case ACCEPTED:
                case PLANT_DISPATCHED:
                    dto.add(linkTo(methodOn(PurchaseOrderRestController.class)
                            .retrievePurchaseOrderExtensions(dto.get_id())).withRel("extend"));
                default:
                    break;
            }
        } catch (Exception e) {
        }

        return dto;

    }
}
