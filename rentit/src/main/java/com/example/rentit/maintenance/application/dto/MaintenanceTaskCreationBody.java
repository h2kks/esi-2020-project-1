package com.example.rentit.maintenance.application.dto;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.maintenance.domain.model.TypeOfWork;
import lombok.Data;

import java.time.LocalDate;

@Data
public class MaintenanceTaskCreationBody {
    private LocalDate endDate;

    private LocalDate startDate;

    private String description;

    private TypeOfWork type;

    private Long reservationId;
}