package com.example.rentit.maintenance.application.dto;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.dto.PlantInventoryItemDTO;
import com.example.rentit.maintenance.domain.model.MOStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class MaintenanceOrderDTO extends RepresentationModel<com.example.rentit.maintenance.application.dto.MaintenanceOrderDTO> {
    Long _id;
    BusinessPeriodDTO maintenancePeriod;
    PlantInventoryItemDTO plant;
    MOStatus status;
    String site;
    String description;
    String engineerName;
}