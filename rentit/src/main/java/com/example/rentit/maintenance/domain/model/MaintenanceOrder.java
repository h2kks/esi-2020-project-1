package com.example.rentit.maintenance.domain.model;

import com.example.rentit.inventory.domain.model.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryItem;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class MaintenanceOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Embedded
    BusinessPeriod maintenancePeriod;

    @OneToOne(cascade = CascadeType.ALL)
    PlantInventoryItem plant;

    @Enumerated(EnumType.STRING)
    MOStatus status;

    String site;
    String description;
    String engineerName;

    public static MaintenanceOrder of(PlantInventoryItem plant, BusinessPeriod period, String site, String description, String engineerName) {
        MaintenanceOrder mo = new MaintenanceOrder();
        mo.plant = plant;
        mo.maintenancePeriod = period;
        mo.status = MOStatus.PENDING;
        mo.site = site;
        mo.description = description;
        mo.engineerName = engineerName;

        return mo;
    }

    public void setStatus(MOStatus status) {
        this.status = status;
    }

    public MOStatus getStatus() {
        return status;
    }

    public BusinessPeriod getMaintenancePeriod() {
        return maintenancePeriod;
    }

    public String getSite() {
        return site;
    }

    public String getDescription() {
        return description;
    }

    public String getEngineerName() {
        return engineerName;
    }

}
